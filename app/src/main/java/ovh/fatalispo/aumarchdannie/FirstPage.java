package ovh.fatalispo.aumarchdannie;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

public class FirstPage extends Activity {

	Button btOiron, btStVarent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.first_page);

		btOiron = findViewById(R.id.btOiron);
		btStVarent = findViewById(R.id.btStVarent);

		btOiron.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				intent.putExtra("url","https://www.aumarchedannie-oiron.fr");
				if(isConnected()) {
					startActivity(intent);
				} else {
					Toast.makeText(getApplicationContext(),"Une connexion internet est nécessaire", Toast.LENGTH_LONG).show();
				}
			}
		});

		btStVarent.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
				intent.putExtra("url","https://www.aumarchedannie-stvarent.fr/");
				if(isConnected()) {
					startActivity(intent);
				} else {
					Toast.makeText(getApplicationContext(),"Une connexion internet est nécessaire", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	private boolean isConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
	}
}
