package ovh.fatalispo.aumarchdannie;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {

	private WebView wb;
	private ProgressBar pbWebview;
	private String urlToDisplay;
	private ImageButton ibPreviousWv,ibNextWv, ibShare;
	private TextView tvUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);

		pbWebview = findViewById(R.id.pbWebview);
		wb = findViewById(R.id.wvSendFeedback);
		tvUrl = findViewById(R.id.tvUrl);
		ibPreviousWv = findViewById(R.id.ibPreviousWv);
		ibNextWv = findViewById(R.id.ibNextWv);
		ibShare = findViewById(R.id.ibShare);

		pbWebview.setVisibility(View.INVISIBLE);
		urlToDisplay = getIntent().getStringExtra("url");

		tvUrl.setText("Au marché d'Annie");
		wb.getSettings().setJavaScriptEnabled(true);
		wb.getSettings().setLoadWithOverviewMode(true);
		wb.getSettings().setUseWideViewPort(true);
		wb.getSettings().setBuiltInZoomControls(true);
		wb.getSettings().setDomStorageEnabled(true);
		wb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
		wb.getSettings().setLoadsImagesAutomatically(true);
		wb.getSettings().setDisplayZoomControls(false);
		wb.getSettings().setBuiltInZoomControls(true);
		wb.setWebViewClient(new WebViewClient(){

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				pbWebview.setVisibility(View.VISIBLE);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
				view.loadUrl(request.getUrl().toString());
				return false;
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				if(pbWebview.getVisibility() != View.INVISIBLE) {
					pbWebview.setVisibility(View.GONE);
				}
			}
		});
		wb.loadUrl(urlToDisplay);

		ibPreviousWv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(wb.canGoBack()) wb.goBack();
			}
		});

		ibNextWv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(wb.canGoForward()) wb.goForward();
			}
		});

		ibShare.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_SEND);
				intent.setType("text/plain");
				intent.putExtra(Intent.EXTRA_TEXT, "Toi aussi, fais tes amplettes Au marché d'Annie "+urlToDisplay);
				startActivity(Intent.createChooser(intent, "Share"));
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(wb != null) {
			wb.clearHistory();
		}
	}
}
